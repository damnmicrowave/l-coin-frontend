import React from 'react';
import PropTypes from 'prop-types';
import {
    Div,
    Panel,
    PanelHeader,
    PanelHeaderClose,
    PopoutWrapper
} from '@vkontakte/vkui/dist/es6/index';

import {Group, Header} from "@vkontakte/vkui/dist/es6";
import {apiRequest, parseNumber, timeUntilMidnight} from "../../utils";

import './styles.scss'

import lCash from "./media/l-cash.svg";


export class Shop extends React.Component {

    buyProduct(productId) {
        const {
            fetchedUser,
            updateData,
            updateProducts,
            setPopout
        } = this.props;
        apiRequest(({data}) => {
            if (data.status === 'success') {
                updateData({cash: data.cash});
                updateProducts(data.products);
                setPopout(<PopoutWrapper><div className="popup">Куплено!</div></PopoutWrapper>)
            } else {
                setPopout(<PopoutWrapper><div className="popup">Недостаточно L-Cash!</div></PopoutWrapper>)
            }
        }, 'buy_product', { vkId: fetchedUser.id, productId })
    }

    render() {
        const {
            id,
            go,
            products,
            playerData
        } = this.props;
        const {
            cash,
            'cash_per_day': cashPerDay
        } = playerData;
        return (
            <Panel id={id}>
                <PanelHeader>
                    <PanelHeaderClose onClick={go} data-to="home"/>
                    Магазин
                </PanelHeader>
                <Div>
                    <div className="header-shop">
                        <div className="block">
                            <img src={lCash} alt="l cash"/>
                            <span>{parseNumber(cash)}</span>
                        </div>
                        {cashPerDay && <div className="block">
                            <div>+{cashPerDay} через: {timeUntilMidnight()}</div>
                        </div>}
                    </div>
                </Div>
                {Object.keys(products).map((category, index) => (
                    <Group key={index} header={<Header mode="secondary">{category}</Header>}>
                            {products[category].sort((a, b) => a.cost - b.cost).map(({id, name, description, cost, count, gradient}) => (
                                <Div key={id}>
                                    <div className="product" onClick={() => {this.buyProduct.bind(this)(id)}} style={{backgroundImage: gradient}}>
                                        <div className="name">{name}</div>
                                        <div className="description">{description}</div>
                                        <div className="cost">Стоимость: {parseNumber(cost * (1.3 ** count))}<img src={lCash} alt="l cash"/></div>
                                    </div>
                                </Div>
                            ))}
                    </Group>
                ))}
                <Div><div className="coming-soon">Со временем магазин будет пополняться</div></Div>
            </Panel>
        );
    }
}

Shop.propTypes = {
    id: PropTypes.string.isRequired,
    go: PropTypes.func.isRequired,
    products: PropTypes.shape({
        id: PropTypes.number.isRequired,
        name: PropTypes.string.isRequired,
        description: PropTypes.string.isRequired,
        cost: PropTypes.number.isRequired,
        count: PropTypes.number.isRequired,
        gradient: PropTypes.string.isRequired,
    }),
    updateData: PropTypes.func.isRequired,
    playerData: PropTypes.shape({
        cash: PropTypes.number.isRequired
    })
};
