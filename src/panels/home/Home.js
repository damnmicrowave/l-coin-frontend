import React from 'react';
import PropTypes from 'prop-types';
import {
    Panel,
    PanelHeader,
    Div,
    CardScroll,
    Card,
    Header,
    Group,
    FixedLayout
} from '@vkontakte/vkui/dist/es6/index';

import './styles.scss'

import lCoin from './media/l-coin.svg';
import lCash from './media/l-cash.svg';
import {apiRequest, parseNumber} from "../../utils";


const removeItem = (obj, key) => {
    delete obj[key];
    return obj
}


export class Home extends React.Component {
    constructor(props) {
        super(props);

        this.state = {
            floaters: {},
            clicks: 0,
            seconds: 0
        }

        this.spawner = React.createRef()
    }

    componentDidMount() {
        this.props.setFunc('updateInterval', () => {
            apiRequest(({data}) => {
                this.props.updateData(data)
            }, 'update_coins', {
                vkId: this.props.fetchedUser.id,
                amountClicked: this.state.clicks,
                secondsPassed: this.state.seconds
            })
            this.setState({clicks: 0, seconds: 0})
        }, 10000);
        this.props.setFunc('cpsInterval', () => {
            const {
                updateData,
                playerData
            } = this.props;
            const {
                coins,
                'coins_per_sec': coinsPerSec
            } = playerData;
            updateData({coins: coins + coinsPerSec})
            this.setState({seconds: this.state.seconds + 1}, () => {
                if (coinsPerSec) this.spawnFloater(coinsPerSec)
            })
        }, 1000)
    }

    buyUpgrade(upgradeIndex) {
        const {
            updateData,
            fetchedUser
        } = this.props;
        apiRequest(({data}) => {
            updateData(data)
        }, 'buy_upgrade', {
            vkId: fetchedUser.id,
            amountClicked: this.state.clicks,
            secondsPassed: this.state.seconds,
            upgradeIndex
        });
        this.setState({clicks: 0, seconds: 0})
    }

    spawnFloater(amount) {
        const {
            floaters,
        } = this.state;
        const {width, height} = this.spawner.current.getBoundingClientRect();
        const top = Math.round(Math.random() * height);
        const left = Math.round(Math.random() * (width - 24));
        const fontSize = Math.round(Math.random() * 20 + 12);
        this.setState({
            floaters: {...floaters, [`${top}-${left}`]: {top, left, fontSize, amount}}
        }, () => {
            setTimeout(() => {
                this.setState({floaters: {...removeItem(this.state.floaters, `${top}-${left}`)}})
            }, 3000)
        })
    }

    click() {
        const {
            clicks,
        } = this.state;
        const {
            coins,
            'coins_per_click': coinsPerClick
        } = this.props.playerData;
        this.setState({
            clicks: clicks + 1,
        }, () => {
            this.spawnFloater(coinsPerClick);
            this.props.updateData({coins: coins + coinsPerClick});
        })
    }

    render() {
        const {
            id,
            fetchedUser,
            go
        } = this.props;
        const {
            floaters,
        } = this.state;
        const {
            coins,
            cash,
            'coins_per_click': coinsPerClick,
            'coins_per_sec': coinsPerSec,
            'upgrade_one': upgradeOne,
            'upgrade_two': upgradeTwo,
            'upgrade_three': upgradeThree,
            'upgrade_four': upgradeFour,
            'upgrade_five': upgradeFive,
            'upgrade_six': upgradeSix,
            'upgrade_seven': upgradeSeven,
        } = this.props.playerData;
        const upgrades = [
            {
                name: 'Штучька',
                effect: '+1/s',
                count: upgradeOne,
                cost: 100
            },
            {
                name: 'Приколюха',
                effect: '+10/s',
                count: upgradeTwo,
                cost: 1000
            },
            {
                name: 'Хреномать',
                effect: '+100/s',
                count: upgradeThree,
                cost: 10000
            },
            {
                name: 'Палец',
                effect: '+1/клик',
                count: upgradeFour,
                cost: 500
            },
            {
                name: 'Рука',
                effect: '+5/клик',
                count: upgradeFive,
                cost: 2500
            },
            {
                name: 'Тася',
                effect: '+100/клик',
                count: upgradeSix,
                cost: 50000
            },
            {
                name: 'Шмудак',
                effect: <>+1<img src={lCash} alt="l cash"/>/d</>,
                count: upgradeSeven,
                cost: 300000
            },
        ];
        const getCost = (cost, count) => Math.ceil(cost * (1.3 ** count));
        return (
            <Panel id={id}>
                <PanelHeader>L-Coin</PanelHeader>
                {fetchedUser && [155755299, 13822147].includes(fetchedUser.id) ?
                    <div style={{display: 'flex', flexDirection: 'column', height: '100%'}}>
                        <Div>
                            <div className="header">
                                <div className="block">
                                    <img src={lCoin} alt="l coin"/>
                                    <span>{parseNumber(coins)}</span>
                                </div>
                                <div className="block">
                                    <img style={{height: 22}} src={lCash} alt="l cash"/>
                                    <span>{parseNumber(cash)}</span>
                                </div>
                                <div className="shop-button" onClick={go} data-to="shop">Магазин</div>
                            </div>
                        </Div>
                        <Group header={<Header mode="secondary">Улучшения</Header>}>
                            <CardScroll>
                                {upgrades.map(({name, effect, count, cost}, index) => (
                                    <Card size="s">
                                        <div
                                            className={`upgrade-card${getCost(cost, count) > coins ? ' disabled' : ''}`}
                                            data-index={index}
                                            onClick={() => {
                                                this.buyUpgrade(index)
                                            }}
                                        >
                                            <div className="name">{name}</div>
                                            <div className="count">{effect} &times; {parseNumber(count)}</div>
                                            <div className="cost">Цена: {parseNumber(getCost(cost, count))}<img
                                                src={lCoin} alt="l coin"/></div>
                                        </div>
                                    </Card>
                                ))}
                            </CardScroll>
                        </Group>
                        <FixedLayout vertical="bottom">
                            <div className="float-area">
                                <div ref={this.spawner} className="spawner">
                                    {Object.keys(floaters).map(key => {
                                        const {top, left, fontSize, amount} = floaters[key];
                                        return <div key={key} className="floater"
                                                    style={{top, left, fontSize}}>+{parseNumber(amount)}</div>
                                    })}
                                </div>
                            </div>
                            <div
                                style={{pointerEvents: coinsPerClick ? 'all' : 'none'}}
                                onClick={this.click.bind(this)}
                                onTouchStart={e => {
                                    e.currentTarget.style.transform = 'scale(.99)';
                                    e.currentTarget.style.boxShadow = '0 0 12px rgba(118, 238, 223, .4)';
                                }}
                                onTouchEnd={e => {
                                    e.currentTarget.style.transform = 'none';
                                    e.currentTarget.style.boxShadow = '0 0 16px rgba(118, 238, 223, .3)';
                                }}
                                className="big-button">
                                <span className="cpc">{parseNumber(coinsPerClick)} <img src={lCoin} alt="l coin"/></span>
                                <span className="cps">+{coinsPerSec}/s</span>
                            </div>
                        </FixedLayout>
                    </div> :
                    <Div>
                        Looks like you're not you
                    </Div>
                }
            </Panel>
        );
    }
}

Home.propTypes = {
    id: PropTypes.string.isRequired,
    go: PropTypes.func.isRequired,
    fetchedUser: PropTypes.shape({
        id: PropTypes.number.isRequired,
    }),
};
