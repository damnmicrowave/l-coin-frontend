export function apiRequest(
    func,
    apiMethod,
    data = {},
) {
    const xhttp = new XMLHttpRequest();
    xhttp.onreadystatechange = function () {
        if (this.readyState === 4 && this.status === 200) {
            const response = JSON.parse(this.responseText);
            func(response)
        }
    };
    const url = `https://${window.location.host}/api/${apiMethod}/`;
    xhttp.open('POST', url, true);
    xhttp.setRequestHeader("Content-Type", "application/json");
    xhttp.send(JSON.stringify(data));
}

export const parseNumber = number => {
    let k = 1;
    let s = 0;
    const suffix = ['', 'k', 'm', 'M'];
    while (number / (k * 1000) >= 1) {
        k *= 1000;
        s++
    }
    return `${Math.round((number / k + Number.EPSILON) * 100) / 100}${suffix[s]}`
}

export const timeUntilMidnight = () => {
    const nextMidnight = new Date();
    nextMidnight.setHours(24,0,0,0);
    const now = new Date();
    let rest = (nextMidnight.getTime() - now.getTime()) / 1000;
    const hours = Math.floor(rest / 3600);
    rest -= hours * 3600;
    const minutes = Math.floor(rest / 60);
    return `${hours < 10 ? '0' : ''}${hours}:${minutes < 10 ? '0' : ''}${minutes}`;
}