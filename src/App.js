import React from 'react';
import bridge from '@vkontakte/vk-bridge';
import View from '@vkontakte/vkui/dist/components/View/View';
import ScreenSpinner from '@vkontakte/vkui/dist/components/ScreenSpinner/ScreenSpinner';
import '@vkontakte/vkui/dist/vkui.css';
import {apiRequest} from './utils'

import {
    Home,
    Shop
} from "./panels";

export class App extends React.Component {

    constructor(props) {
        super(props);

        this.state = {
            activePanel: 'home',
            popout: null,
            fetchedUser: null,
            playerData: null,
            products: null,
            loaded: false,
            cpsInterval: setInterval(() => {}, 1000),
            updateInterval: setInterval(() => {}, 10000)
        }
    }

    updateData(obj) {
        this.setState({playerData: {...this.state.playerData, ...obj}})
    }

    updateProducts(obj) {
        this.setState({products: {...this.state.products, ...obj}})
    }

    async fetchData() {
        const user = await bridge.send('VKWebAppGetUserInfo');
        apiRequest(({data, products}) => {
            this.setState({
                fetchedUser: user,
                playerData: data,
                products,
                loaded: true
            })
        }, 'init', {vkId: user.id})
    }

    componentDidMount() {
        this.fetchData().then(r => console.log(r));
        setInterval(this.state.cpsInterval, 1000);
        setInterval(this.state.updateInterval, 10000);
    }

    setFunc(type, func, timeout) {
        clearInterval(this.state[type])
        this.setState({[type]: setInterval(func, timeout)})
    }

    render() {
        const {
            activePanel,
            fetchedUser,
            popout,
            playerData,
            products,
            loaded
        } = this.state;
        const go = e => {
            this.setState({activePanel: e.currentTarget.dataset.to})
        }
        return loaded ? <View activePanel={activePanel} popout={popout}>
            <Home
                id='home'
                fetchedUser={fetchedUser}
                playerData={playerData}
                go={go}
                updateData={this.updateData.bind(this)}
                setFunc={this.setFunc.bind(this)}
            />
            <Shop
                id='shop'
                go={go}
                products={products}
                updateData={this.updateData.bind(this)}
                updateProducts={this.updateProducts.bind(this)}
                fetchedUser={fetchedUser}
                playerData={playerData}
                setPopout={popout => {this.setState({popout}, () => {setTimeout(() => {this.setState({popout: null})}, 1000)})}}
            />
        </View> : <ScreenSpinner size='large'/>;
    }
}
